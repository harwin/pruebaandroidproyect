package com.example.albunes_administrador;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.albunes_administrador.DB.OperacionesDb;
import com.example.albunes_administrador.Modelos.usuarioModel;

import androidx.appcompat.app.AppCompatActivity;

public class createUsuarioActivity extends AppCompatActivity {
    EditText user, pass;
    // private   file;
    OperacionesDb datos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_usuario);

        final Button guard = (Button) findViewById(R.id.btnGuarUser);
        guard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               popuGuardar();
            }
        });
    }

   public boolean  popuGuardar(){

       final EditText mailUno = (EditText) findViewById(R.id.inputMailNewone);
       final EditText mailDos = (EditText) findViewById(R.id.numeroAlbun);
       final EditText claveUno = (EditText) findViewById(R.id.inputClaveregistration);
       final EditText claveDos = (EditText) findViewById(R.id.inputClaveregistrationdos);
       Dialog dialogo =new Dialog(createUsuarioActivity.this);

       getApplicationContext().deleteDatabase("albunes.db");
       datos = OperacionesDb.obtenerInstancia(getApplicationContext());

       try {
           if(mailUno== mailDos && claveUno==claveDos){
               datos.insertarUsuario(new usuarioModel(mailDos.getText().toString(), claveUno.getText().toString()));
               datos.getDb().beginTransaction();
               usuarioCreadoRedire();
               return true;
           }
       } finally {
           usuarioCreadoRedire();
           datos.getDb().endTransaction();
           return false;
       }


    }

    public  void  usuarioCreadoRedire(){
        Intent inicialIten = new Intent(this, MainActivity.class);
        startActivity(inicialIten);
    }
}

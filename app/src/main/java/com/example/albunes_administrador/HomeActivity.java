package com.example.albunes_administrador;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;

import com.example.albunes_administrador.adapter.albumAdapter;
import com.example.albunes_administrador.consumer.WebServicce;
import com.example.albunes_administrador.igurmModel.Data;
import com.example.albunes_administrador.igurmModel.imgurImagen;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity {

    Button btnBuscar;
    Button btnCrearALbun;
    Button btnCrearPerfil;
    EditText TextAbuscar;
   private RecyclerView recyclerView;
   private albumAdapter ImagesAdapter;
   private List<imgurImagen> image;



   private Retrofit retrofit;
    private HttpLoggingInterceptor  loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setUpView();
        lanzarPeticion();

        final CheckedTextView cerrar = (CheckedTextView) findViewById(R.id.cerrarSession);
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSession();
            }
        });


        final Button btnCreateUsuario = (Button) findViewById(R.id.btnPerfilNew);
        btnCreateUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popuCrearNuevoPerfil();
            }
        });

     final   Button btnCrearAlbun = (Button) findViewById(R.id.btnCrearAlbun);
        btnCrearAlbun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popuCrearNuevoAlbun();
            }
        });

        final Button btnPerfilnew = (Button) findViewById(R.id.btnPerfilNew);
        btnPerfilnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popuCrearNuevoPerfil();
            }
        });
    }


   private void lanzarPeticion(){
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.imgur.com/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

       WebServicce client= retrofit.create(WebServicce.class);
       Call<Data> call = client.getAlbun();
       call.enqueue(new Callback<Data>() {
           @Override
           public void onResponse(Call<Data> call, Response<Data> response) {

               //ImagesAdapter.setData(response.body().getResults());
               System.out.print(response.body().getResults());
           }

           @Override
           public void onFailure(Call<Data> call, Throwable t) {
               Log.d("TAG1", "Error: " + t.getMessage());
           }
       });

    }

    private  void setUpView(){
        image = new ArrayList<>();
        ImagesAdapter= new albumAdapter(image);
        recyclerView =findViewById(R.id.recyclerView);
        LinearLayoutManager lim= new LinearLayoutManager(this);
        lim.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(lim);
        recyclerView.setAdapter(ImagesAdapter);
    }





    public  void  cerrarSession(){
        Intent inicialIten = new Intent(this, MainActivity.class);
        startActivity(inicialIten);
    }

    public  void  popuCrearNuevoPerfil(){

        Dialog dialogo = new Dialog(HomeActivity.this);
        dialogo.setTitle("Nuevo Usuario");
        dialogo.setCancelable(true);
        dialogo.setContentView(R.layout.activity_create_perfil);
        dialogo.show();
    }

    public void setUpView(Button btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public  void  popuCrearNuevoAlbun(){

        Dialog dialogo = new Dialog(HomeActivity.this);
        dialogo.setTitle("Nuevo Usuario");
        dialogo.setCancelable(true);
        dialogo.setContentView(R.layout.activity_create_albun);
        dialogo.show();
    }

}

package com.example.albunes_administrador.consumer;


import com.example.albunes_administrador.igurmModel.Data;
import com.example.albunes_administrador.igurmModel.ImageResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface WebServicce {
    @GET("/3/album/11CkR5K/authorize?client_id=3deb98feeca5884&response_type=REQUESTED_RESPONSE_TYPE")
    Call<Data> getAlbun();

    @GET()
    Call<ImageResponse> getAlbun(@Url String url);

}

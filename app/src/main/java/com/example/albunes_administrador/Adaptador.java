package com.example.albunes_administrador;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import  android.view.View;
import  android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;


import com.example.albunes_administrador.Modelos.usuarioModel;

import java.time.Instant;
import java.util.ArrayList;

 public class Adaptador extends BaseAdapter {

   ArrayList<usuarioModel> listaUs;
   albunesDB objAlbDb;
   usuarioModel u;
   Activity a;

     public Adaptador(ArrayList<usuarioModel> listaUs, albunesDB objAlbDb, usuarioModel u, Activity a) {
         this.listaUs = listaUs;
         this.objAlbDb = objAlbDb;
         this.u = u;
         this.a = a;
     }

     @Override
     public int getCount() {
         return 0;
     }

     @Override
     public Object getItem(int position) {
         return null;
     }

     @Override
     public long getItemId(int position) {
         return 0;
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
         View ve= convertView;
         if(ve!=null){
             LayoutInflater ini = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             ve=ini.inflate(R.layout.activity_main, null );
         }
          u= listaUs.get(position);
         Button login = (Button)ve.findViewById(R.id.btn_login);
         login.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 //Validar login he ir a home

             }
         });


         return ve;
     }
 }

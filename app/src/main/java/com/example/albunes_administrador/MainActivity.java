package com.example.albunes_administrador;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.albunes_administrador.DB.OperacionesDb;
import com.example.albunes_administrador.Modelos.usuarioModel;

import java.util.Calendar;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    EditText user, pass;
    // private   file;
    OperacionesDb datos;

    public class TareaPruebaDatos extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // [INSERCIONES]
            String fechaActual = Calendar.getInstance().getTime().toString();

            try {
                datos.insertarUsuario(new usuarioModel("harwingalvis@gmail.com", "12345678"));
                datos.getDb().beginTransaction();

            } finally {
               datos.getDb().endTransaction();
            }

            // [QUERIES]
            Log.d("usuarios","usuarios");
            //DatabaseUtils.dumpCursor(datos.obtenerUsuario();

            return null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // user=findViewById(R.id.class);


        final Button login = (Button) findViewById(R.id.btn_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

        final Button btnCreateUsuario = (Button) findViewById(R.id.btnPerfilNew);
        btnCreateUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popuCrearNuevoUsuario();
            }
        });




    }



    public  void Login(){

        getApplicationContext().deleteDatabase("albunes.db");
        datos = OperacionesDb.obtenerInstancia(getApplicationContext());
        new TareaPruebaDatos().execute();
        Button login = (Button) findViewById(R.id.btn_login);
        Button create = (Button) findViewById(R.id.btnPerfilNew);

        final EditText mail= findViewById(R.id.mailLogin);
        final EditText clave= findViewById(R.id.claveLogin);

        Cursor usuario = datos.obtenerUsuario(mail.getText().toString(), clave.getText().toString());
        String res= usuario.toString();

        if(usuario.getCount() > 0){
            Intent inicialIten = new Intent(this, HomeActivity.class);
            startActivity(inicialIten);
        }else{

            popuCrearNuevoUsuario();

        }

    }

    public  void  popuCrearNuevoUsuario(){

        Dialog dialogo = new Dialog(MainActivity.this);
        dialogo.setTitle("Nuevo Usuario");
        dialogo.setCancelable(true);
        dialogo.setContentView(R.layout.activity_create_usuario);
        dialogo.show();
    }




}


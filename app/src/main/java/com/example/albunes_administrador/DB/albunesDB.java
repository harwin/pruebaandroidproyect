package com.example.albunes_administrador.DB;
import com.example.albunes_administrador.Modelos.*;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.example.albunes_administrador.DB.contratoDb.usuariosColumnas;
import com.example.albunes_administrador.DB.contratoDb.PerfilColumnas;
import com.example.albunes_administrador.DB.contratoDb.albunColumnas;
import com.example.albunes_administrador.DB.contratoDb.imagenColumnas;
import com.example.albunes_administrador.DB.contratoDb.estadoColumnas;
import com.example.albunes_administrador.DB.contratoDb.Tablas;
import android.provider.BaseColumns;

public class albunesDB extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "albunes.db";

    private final Context contexto;
    
    public albunesDB(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        contexto = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }
    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT NOT NULL)",
                Tablas.estados,  BaseColumns._ID,
                estadoColumnas.estado));


        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT NOT NULL,%s TEXT NOT NULL )",
                           Tablas.usuarios,  BaseColumns._ID,
                                usuariosColumnas.usuario,
                                 usuariosColumnas.clave));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL)",
                Tablas.perfil,  BaseColumns._ID,
                PerfilColumnas.nombreCompleto,
                PerfilColumnas.direcion,
                PerfilColumnas.telefono,
                PerfilColumnas.linkFaceboo));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT  NOT NULL,%s TEXT NOT NULL,%s INTEGER NOT NULL,%s INTEGER NOT NULL," +
                        "FOREIGN KEY(idPerfil) REFERENCES  perfil(_ID), FOREIGN KEY(idEstado) REFERENCES  estados(_ID))",

                Tablas.albun,  BaseColumns._ID,
                albunColumnas.nombreAlbun,
                albunColumnas.typo,
                albunColumnas.idPerfil,
                albunColumnas.idEstado));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s INTEGER  NOT NULL,%s TEXT  NOT NULL,%s INTEGER NOT NULL," +
                        "FOREIGN KEY(idEstado) REFERENCES  estados(_ID))",
                Tablas.imagen,  BaseColumns._ID,
                imagenColumnas.nombreImagen,
                imagenColumnas.urlImag,
                imagenColumnas.idEstado));





    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Tablas.usuarios);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.perfil);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.albun);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.imagen);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.estados);

        onCreate(db);
    }


}


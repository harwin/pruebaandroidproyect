package com.example.albunes_administrador.DB;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.albunes_administrador.DB.contratoDb.*;

import com.example.albunes_administrador.Modelos.usuarioModel;
import com.example.albunes_administrador.Modelos.perfilModel;
import com.example.albunes_administrador.Modelos.albunModel;
import com.example.albunes_administrador.Modelos.imagenModel;
import com.example.albunes_administrador.Modelos.estadosModel;
import com.example.albunes_administrador.DB.albunesDB;

public final class OperacionesDb {

    private static albunesDB baseDatos;

    private static OperacionesDb instancia = new OperacionesDb();

    private OperacionesDb() {
    }

    public static OperacionesDb obtenerInstancia(Context contexto) {
        if (baseDatos == null) {
            baseDatos = new albunesDB(contexto);
        }
        return instancia;
    }

    // [OPERACIONES_USUARIO]
    public Cursor obtenerUsuario(String mail, String password) {

        SQLiteDatabase db = baseDatos.getReadableDatabase();
        usuarioModel objUsu= new usuarioModel();
        String sql = String.format("SELECT * FROM "+contratoDb.Tablas.usuarios.toString()+" where usuario = '"+mail.toString()+"' and clave = '"+ password.toString()+"'");
        return db.rawQuery(sql, null);
    }

    public String insertarUsuario(usuarioModel usuarioModel) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        // Generar Pk


        valores.put(usuariosColumnas.usuario, usuarioModel.usuario);
        valores.put(usuariosColumnas.clave, usuarioModel.clave);

        db.insertOrThrow(contratoDb.Tablas.usuarios, null, valores);

        return  usuarioModel.getUsuario();

    }


    public SQLiteDatabase getDb() {
        return baseDatos.getWritableDatabase();
    }
}
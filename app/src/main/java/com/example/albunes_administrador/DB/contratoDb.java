package com.example.albunes_administrador.DB;

public class contratoDb {
    public  class usuariosColumnas{
        //columnas Tabla.
        public  static  final String  usuario= "usuario";
        public   static final String  clave= "clave";
    }

    public  class PerfilColumnas{
        //columnas Tabla.
        public  static  final String   nombreCompleto= "nombreCompleto";
        public  static  final String   direcion= "direcion";
        public   static final String   telefono= "telefono";
        public static final String     linkFaceboo ="usuarios";

    }

    public  class albunColumnas{
        //columnas Tabla.
        public  static  final String    nombreAlbun= "nombreAlbun";
        public  static  final String    typo= "typo";
        public   static final String    idPerfil= "idPerfil";
        public static final String      idEstado ="idEstado";

    }

    public  class imagenColumnas{
        //columnas Tabla.
        public  static  final String    nombreImagen= "nameImagen";
        public  static  final String    tipo= "typo";
        public   static final String    urlImag= "urlImag";
        public static final String      idEstado ="idEstado";

    }
    public  class estadoColumnas{
        //columnas Tabla.
        public  static  final String    estado= "estado";
    }

    interface Tablas {
        String usuarios = "usuarios";
        String perfil = "perfil";
        String imagen = "imagen";
        String albun = "albun";
        String estados = "estados";
    }

}

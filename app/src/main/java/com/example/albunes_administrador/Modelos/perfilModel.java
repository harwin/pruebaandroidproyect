package com.example.albunes_administrador.Modelos;

public class perfilModel {



    String nombreCompleto;
    String correoElectronico;
    int telefono;
    String direcciont;
    int idUsuario;

    public perfilModel( String nombreCompleto, String correoElectronico, int telefono, String direcciont, int idUsuario) {

        this.nombreCompleto = nombreCompleto;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
        this.direcciont = direcciont;
        this.idUsuario = idUsuario;
    }



    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDirecciont() {
        return direcciont;
    }

    public void setDirecciont(String direcciont) {
        this.direcciont = direcciont;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}

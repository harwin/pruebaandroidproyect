package com.example.albunes_administrador.Modelos;

public class albunModel {
    int idAlbun;
    String nombreAlbun;
    int    idestado;

    public albunModel(int idAlbun, String nombreAlbun, int idestado) {
        this.idAlbun = idAlbun;
        this.nombreAlbun = nombreAlbun;
        this.idestado = idestado;
    }

    public int getIdAlbun() {
        return idAlbun;
    }

    public void setIdAlbun(int idAlbun) {
        this.idAlbun = idAlbun;
    }

    public String getNombreAlbun() {
        return nombreAlbun;
    }

    public void setNombreAlbun(String nombreAlbun) {
        this.nombreAlbun = nombreAlbun;
    }

    public int getIdestado() {
        return idestado;
    }

    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }
}

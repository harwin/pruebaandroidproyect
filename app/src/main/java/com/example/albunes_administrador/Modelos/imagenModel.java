package com.example.albunes_administrador.Modelos;

public class imagenModel {

    String  nameImagen;
    String urlImage;
    int    idEstado;
    int    idAlbun;

    public imagenModel( String nameImagen, String urlImage, int idEstado, int idAlbun) {

        this.nameImagen = nameImagen;
        this.urlImage = urlImage;
        this.idEstado = idEstado;
        this.idAlbun = idAlbun;
    }


    public String getNameImagen() {
        return nameImagen;
    }

    public void setNameImagen(String nameImagen) {
        this.nameImagen = nameImagen;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public int getIdAlbun() {
        return idAlbun;
    }

    public void setIdAlbun(int idAlbun) {
        this.idAlbun = idAlbun;
    }
}

package com.example.albunes_administrador.Modelos;

public class usuarioModel {
   public   String usuario;
   public   String clave;



    public usuarioModel(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }

    public usuarioModel() {

    }



    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}

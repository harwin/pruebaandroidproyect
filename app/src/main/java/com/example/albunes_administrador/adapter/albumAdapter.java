package com.example.albunes_administrador.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.albunes_administrador.R;
import com.example.albunes_administrador.igurmModel.imgurImagen;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class albumAdapter  extends RecyclerView.Adapter<albumAdapter.AlbunAdapterHolder> {

     private List<imgurImagen> ImageList;

    public albumAdapter( List<imgurImagen> ImageLists ){
         this.ImageList=ImageLists;
    }

    @NonNull
    @Override
    public AlbunAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_list_image, parent, false);

        return new AlbunAdapterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbunAdapterHolder holder, int position) {
        imgurImagen albun= ImageList.get(position);
        holder.SizeImag.setText(albun.getSize());
        holder.typeImag.setText(albun.getLink());
        holder.ImName.setText(albun.getName());
        holder.ImName.setText(albun.getLink());


    }

    @Override
    public int getItemCount() {
        return ImageList.size();
    }


    public void setData(List<imgurImagen> image){
        this.ImageList = image;
        notifyDataSetChanged();
    }

    public  class AlbunAdapterHolder extends RecyclerView.ViewHolder {

        private TextView ImName;
        private TextView SizeImag;
        private TextView typeImag;
        private TextView LinkImag;

        public AlbunAdapterHolder(@NonNull View itemView, TextView sizeImag, TextView typeImag, TextView imName,  TextView linkImag) {
            super(itemView);
            SizeImag = sizeImag;
            this.typeImag = typeImag;
            this.ImName=imName;
            this.LinkImag=linkImag;
        }



        public AlbunAdapterHolder(@NonNull View itemView) {
            super(itemView);

            ImName = itemView.findViewById(R.id.tvNameImage);
            typeImag = itemView.findViewById(R.id.tvTextImage);
            SizeImag =itemView.findViewById(R.id.tvSize);
             LinkImag= itemView.findViewById(R.id.ImgUrl);


        }

    }

}

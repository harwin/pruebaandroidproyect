package com.example.albunes_administrador.cliente.utilidades;

import android.util.Log;

import com.example.albunes_administrador.Constantes;

/**
 * Created by AKiniyalocts on 1/16/15.
 *
 * Basic logger bound to a flag in Constants.java
 */
public class aLog {
    public static void w (String TAG, String msg){
        if(Constantes.LOGGING) {
            if (TAG != null && msg != null)
                Log.w(TAG, msg);
        }
    }

}